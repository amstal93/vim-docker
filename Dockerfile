FROM alpine:latest

ENV USER=vimuser \
  MOUNT_DIR=/app 

RUN apk add --no-cache \
  neovim \
  git \
  curl \
  nodejs \
  npm \
  python3 \
  py3-pip 

# Make a sudo capable $USER
RUN mkdir -p $MOUNT_DIR \ 
  && mkdir -p /etc/sudoers.d/ \
  && adduser -D $USER \
  -h /home/$USER \
  --disabled-password \
  && chown $USER:$USER $MOUNT_DIR \
  && echo "$USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER \
  && chmod 0440 /etc/sudoers.d/$USER

USER $USER
WORKDIR /home/$USER
# Install VimPlug
RUN curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

COPY --chown=$USER:$USER .vimrc .vimrc
#COPY --chown=$USER:$USER ./vimplug/* /home/$USER/
COPY --chown=$USER:$USER ./vimplug/.cache .cache
COPY --chown=$USER:$USER ./vimplug/.config .config
COPY --chown=$USER:$USER ./vimplug/.local .local
COPY --chown=$USER:$USER ./vimplug/.npm .npm
COPY --chown=$USER:$USER ./vimplug/.vim .vim

# Install all vimplug extensions so everything just boots up as needed
RUN nvim -E -c 'PlugInstall' -c q ; return 0

ENV TERM=xterm-256color
WORKDIR $MOUNT_DIR

ENTRYPOINT [ "sh", "-c", "sleep 0.1; nvim $0 $@" ]

