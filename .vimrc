"set rtp+=$GOROOT/misc/vim"
filetype plugin indent on
syntax on
set nobackup
set nowritebackup
" Give more space for displaying messages.
set cmdheight=2
" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300
" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Set the leaderkey to , instead of \
let mapleader = ","

" Set split separator to Unicode box drawing character
set encoding=utf8
set fillchars=vert:│

" Override color scheme to make split the same color as tmux's default
autocmd ColorScheme * highlight VertSplit cterm=NONE ctermfg=Green ctermbg=NONE
hi VertSplit cterm=NONE

if has('nvim')
    " Install Vim-Plug if needed
    if empty(glob('~/.vim/autoload/plug.vim'))
      silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
      autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
    endif
    " Neovim specific commands
    call plug#begin()
      Plug 'https://github.com/tpope/vim-sleuth'
        Plug 'tpope/vim-sensible'
        Plug 'https://github.com/tpope/vim-surround'
        Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
        Plug 'scrooloose/nerdcommenter'
        "Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
        Plug 'junegunn/fzf'
        Plug 'BurntSushi/ripgrep'
        Plug 'https://github.com/itchyny/lightline.vim'
        Plug 'https://github.com/terryma/vim-multiple-cursors'
        Plug 'neoclide/coc.nvim', {'branch': 'release'}
        Plug 'sheerun/vim-polyglot'
        Plug 'prettier/vim-prettier'
        Plug 'ctrlpvim/ctrlp.vim'


    call plug#end()
    " Configure TS Server for CoC
    let g:coc_global_extensions = [
      \ 'coc-tsserver',
      \ 'coc-pyright'
      \ ]

    "let g:go_version_warning = 0
    " Map ctrl-b to toggle nerdtree
    map <C-b> :NERDTreeToggle<CR>
    " Map Ctrl-J, etc to the window split movements
    nnoremap <C-J> <C-W><C-J>
    nnoremap <C-K> <C-W><C-K>
    nnoremap <C-L> <C-W><C-L>
    nnoremap <C-H> <C-W><C-H>
    " Configure CoC
    " More examples here: https://github.com/neoclide/coc.nvim#example-vim-configuration
    " use <c-space> to trigger completion
    inoremap <silent><expr> <c-space> coc#refresh()
    " Use K to show documentation in preview window.
    nnoremap <silent> K :call <SID>show_documentation()<CR>
    function! s:check_back_space() abort
      let col = col('.') - 1
      return !col || getline('.')[col - 1]  =~# '\s'
    endfunction
    " Use tab for trigger completion with characters ahead and navigate.
    " NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
    " other plugin before putting this into your config.
    inoremap <silent><expr> <TAB>
          \ pumvisible() ? "\<C-n>" :
          \ <SID>check_back_space() ? "\<TAB>" :
          \ coc#refresh()
    inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

    autocmd CursorHold * silent call CocActionAsync('highlight')

    function! s:show_documentation()
      if (index(['vim','help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
      else
        call CocAction('doHover')
      endif
    endfunction
    " Formatting selected code.
    xmap <leader>f  <Plug>(coc-format-selected)
    " Add `:Format` command to format current buffer.
    command! -nargs=0 Format :call CocAction('format')
    " Add `:OR` command for organize imports of the current buffer.
    command! -nargs=0 OrganizeImports   :call     CocAction('runCommand', 'editor.action.organizeImport')
    " Mappings for CoCList
    " Show all diagnostics.
    nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
    " Manage extensions.
    nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
    " Show commands.
    nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
    " Find symbol of current document.
    nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
    " Search workspace symbols.
    nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
    " Do default action for next item.
    nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
    " Do default action for previous item.
    nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
    " Resume latest coc list.
    nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
    " GoTo code navigation.
    nmap <silent> gd <Plug>(coc-definition)
    nmap <silent> gy <Plug>(coc-type-definition)
    nmap <silent> gi <Plug>(coc-implementation)
    nmap <silent> gr <Plug>(coc-references)
    " Remap keys for applying codeAction to the current line.
    nmap <leader>ac  <Plug>(coc-codeaction)
    " Apply AutoFix to problem on the current line.
    nmap <leader>af  <Plug>(coc-fix-current)

    " Configure Ctrl-P
    let g:ctrlp_map = '<c-p>'
    let g:ctrlp_cmd = 'CtrlP'
    let g:ctrlp_working_path_mode = 'ra'

    " Add additional root markers:
    let g:ctrlp_root_markers = ['package.json', 'composer.json']
    " Ignore files in .gitignore
    let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']
    " Ignore minified JS
    set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.min.js     " MacOSX/Linux
    "let g:ctrlp_user_command = 'find %s -type f'        " MacOSX/Linux

    let g:ctrlp_custom_ignore = {
      \ 'dir':  '\v[\/](doc|tmp|node_modules)',
      \ 'file': '\v\.(exe|so|dll|node_modules)$',
      \ }

    " Configure Nerd Commenter
    " Add spaces after comment delimiters by default
    let g:NERDSpaceDelims = 1

    " Use compact syntax for prettified multi-line comments
    let g:NERDCompactSexyComs = 1

    " Align line-wise comment delimiters flush left instead of following code indentation
    let g:NERDDefaultAlign = 'left'
    nnoremap <leader>t :NERDTreeToggle<CR>
    nnoremap <C-n> :NERDTreeFocus<CR>
    nnoremap <C-t> :NERDTreeToggle<CR>
    nnoremap <C-f> :NERDTreeFind<CR>

    "Configure tab shortcuts
    nnoremap <C-Left> :tabprevious<CR>
    nnoremap <C-Right> :tabnext<CR>


    " Remap VIM window management to use tmux-like splits
    nnoremap <Leader>w <C-w>
    nnoremap <C-w>% <C-w><C-v>
    nnoremap <C-w>" <C-w><C-s>
    " Set splits to feel more natural
    set splitbelow
    set splitright
    " And simplify movement
    nnoremap <C-J> <C-W><C-J>
    nnoremap <C-K> <C-W><C-K>
    nnoremap <C-L> <C-W><C-L>
    nnoremap <C-H> <C-W><C-H>

    " Set tabs to 2
    set tabstop=2 shiftwidth=2 expandtab

else
    " Standard vim specific commands
endif

